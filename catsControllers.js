const catsRepo = require('./catsRepository');

module.exports = {
    getAllController(req, res, next) {
        const cats = catsRepo.getAll();
        return res.json(cats);
    },

    getByIdController(req, res, next) {
        const catId = req.params.catId;
        const cat = catsRepo.get(Number(catId));
        if (!cat) {
            res.statusCode = 404;
            return res.json({ message: 'Not Found' });
        }

        return res.json(cat);
    },

    createController(req, res, next) {
        const rawCatData = req.body;
        const cat = catsRepo.create(rawCatData);

        if (!cat) {
            res.statusCode = 403;
            return res.json({ message: 'Invalid data' });
        }

        return res.json(cat);
    }
};