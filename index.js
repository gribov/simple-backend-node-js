const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');

const router = require('./router');

// init App
const app = express();

// CORS setup
const FRONTEND_APP_HOST = 'http://localhost:8080';
app.use(cors({ origin: FRONTEND_APP_HOST }));

//
app.use(bodyParser.json({ type: '*/*' }));

router(app);

//Server setup
const port = 3001;
const server = http.createServer(app);
server.listen(port);
console.log('SERVER LISTENING ON http://localhost:' + port);
