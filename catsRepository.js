const allCats = [
    new Cat(1, 'pussy_1', 3),
    new Cat(2, 'pussy_2', 3),
    new Cat(3, 'pussy_3', 3),
    new Cat(4, 'pussy_4', 3),
];

function Cat(id, name, age) {
    this.id = id;
    this.name = name;
    this.age = age;
}

module.exports = {
    get(id) {
        return allCats.find(cat => cat.id === id);
    },
    getAll() {
        return allCats;
    },
    create(data) {
        const { name, age } = data;
        if (
            name
            && name.length
            && age
            && age > 0
        ) {
            const id = allCats.length + 1;
            const newCat = new Cat(id, name, age);
            // DB save
            allCats.push(newCat);
            return newCat;
        }

        return null;
    }
}