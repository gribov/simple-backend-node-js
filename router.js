
const controllers = require('./catsControllers');

function router(app) {

    app.get('/', function(req, res, next) {
        return res.json('ok');
    });

    app.get('/api/v1/cats', controllers.getAllController);

    app.get('/api/v1/cats/:catId', controllers.getByIdController);

    app.post('/api/v1/cats', controllers.createController);
};

module.exports = router;
